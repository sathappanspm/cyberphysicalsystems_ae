#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
    *.py: Description of what * does.
    Last Modified:
"""

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"

import numpy as np
import pandas as pd
from RDAE import Deep_Autoencoder, RobustL21Autoencoder, RDAE, Sparse_Deep_Autoencoder, Sparse_Dropout_Autoencoder
import os
import tensorflow as tf
import glob


def train_Deep_Autoencoder(data, sess, num_epochs, input_dims):
    # model = Sparse_Deep_Autoencoder(sess, input_dim_list=input_dims, sparsities=[.5]*(len(input_dims) - 1))
    model = Deep_Autoencoder(sess, input_dim_list=input_dims)
    model.fit(data, sess=sess, learning_rate=0.01,
              batch_size=data.shape[0], verbose=False, iteration=num_epochs)
    return model


def train_RobustL21Autoencoder(data, sess, num_epochs):
    model = RobustL21Autoencoder(sess=sess, lambda_=40, layers_sizes=[32, 20])
    model.fit(data, sess=sess, inner_iteration=num_epochs,
              iteration=5, batch_size=data.shape[0], verbose=False)
    return model


def train_RDAE(data, sess, num_epochs):
    model = RDAE(sess=sess, lambda_=10, layers_sizes=[32, 20])
    model.fit(data, sess=sess, learning_rate=0.01, batch_size=data.shape[0],
              inner_iteration=num_epochs, iteration=5, verbose=False)
    return model

def main(args):
    algo = globals()[args.algo]
    fl = glob.glob(os.path.join(args.datadir, "nois*.csv"))[0]

    basefilename = os.path.basename(fl)
    df = pd.read_csv(fl)
    dfmat = df.as_matrix()
    print(df.shape, basefilename)
    splitindex = int(dfmat.shape[0] * args.trainsplit)
    valset = int(splitindex * 0.01)
    print("total train size:{}, valsize:{}".format(splitindex, valset))
    trainmat, testmat = dfmat[:(splitindex - valset), :], dfmat[(splitindex - valset): splitindex, :]
    print("create tf session")
    params = [[32, 28, 20], [32, 28], [32, 28, 20, 10], [32, 20], [32, 10]]
    for input_dims in params:
        print("Error for param - ", input_dims)
        tf.reset_default_graph()
        with tf.Session() as sess:
            init = tf.global_variables_initializer()
            init.run(session=sess)

            model = globals()['train_{}'.format(algo.__name__)](trainmat, sess, args.iter, input_dims) 
            if algo.__name__ != 'Deep_Autoencoder':
                reconstructed = model.AE.getRecon(dfmat, sess)
            else:
                reconstructed = model.getRecon(dfmat, sess)
           
            errormat = (reconstructed[splitindex - valset: splitindex, :] - dfmat[splitindex - valset: splitindex, :])
            print("MSE: Mean-{:.2f} , Max-{:.2f}".format((errormat ** 2).mean(), (errormat ** 2).mean(axis=1).max()))
            print("ABS: Mean-{:.2f} , Max-{:.2f}".format(np.abs(errormat).mean(), np.abs(errormat).mean(axis=1).max()))
            



if __name__ == "__main__":
    import argparse
    from datetime import datetime
    ap = argparse.ArgumentParser()
    ap.add_argument("--datadir", help='input data directory', default='../../data/ieee_33_bus_dataset/')
    ap.add_argument("--iter", type=int, help='number of epochs to run')
    ap.add_argument("--trainsplit", type=float, help="train percentage")
    ap.add_argument("--algo", help="which algorithm to create")
    args = ap.parse_args()
    main(args)
