#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
    *.py: Description of what * does.
    Last Modified:
"""

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"

import numpy as np
import pandas as pd
from RDAE import Deep_Autoencoder, RobustL21Autoencoder, RDAE
import os
import tensorflow as tf
import glob
import json
# import ipdb

def to_str(array):
    return '-'.join([str(item) for item in array])

def train_Deep_Autoencoder(data, sess, num_epochs, dims=None, l1param=None):
    model = Deep_Autoencoder(sess, input_dim_list=dims)
    print("batch size is", data.shape[0])
    model.fit(data, sess=sess, learning_rate=0.01,
              batch_size=50, verbose=True, iteration=num_epochs)
    return model


def train_RobustL21Autoencoder(data, sess, num_epochs, dims=None, l1param=None):
    model = RobustL21Autoencoder(sess=sess, lambda_=l1param, layers_sizes=dims)
    model.fit(data, sess=sess, inner_iteration=num_epochs,
              iteration=5, batch_size=data.shape[0], verbose=False)
    return model


def train_RDAE(data, sess, num_epochs, dims=None, l1param=None):
    model = RDAE(sess=sess, lambda_=l1param, layers_sizes=dims)
    model.fit(data, sess=sess, learning_rate=0.01, batch_size=data.shape[0],
              inner_iteration=num_epochs, iteration=5, verbose=False)
    return model


def rolling_window(mat, window=200):
    # new shape is (numberOfWindows * WindowSize * (numberOfVariables i.e., columnsize))
    newshape = (mat.shape[0] - window + 1,) + (window, mat.shape[-1])
    # number of strides to be made in the original matrix to reach next index in axis with other axises fixed
    newstride = (mat.strides[0], mat.strides[0], mat.strides[-1])
    return np.lib.stride_tricks.as_strided(mat, shape=newshape, strides=newstride, writeable=False)


def scale_transform(data):
    dmin = data.min(axis=1)
    dmax = data.max(axis=1)
    datagap = dmax - dmin
    return (data - dmin[:, np.newaxis, :])/datagap[:, np.newaxis, :], dmin, datagap


def reverse_scale(data, dmin, dgap):
    return data * dgap[:, np.newaxis, :] + dmin[:, np.newaxis, :]


def create_windowed_flattened_matrices(mat, window=200, shuffle=True):
    winmat = rolling_window(mat, window)
    winflat = winmat.reshape([winmat.shape[0], -1])
    if shuffle:
        np.random.shuffle(winflat)
    return winflat

def next_batch(x_input, batch_size):
    total_num_items = x_input.shape[0]
    shuffled_list = np.arange(total_num_items)
    np.random.shuffle(shuffled_list)
    for rr in range(0, total_num_items, batch_size):
        yield x_input[shuffled_list[rr: rr + batch_size]]


def main(args):
    if args.algo == 'all':
        algorithms = [Deep_Autoencoder, RobustL21Autoencoder, RDAE]
    else:
        algorithms = [globals()[args.algo]]

    files = glob.glob(os.path.join(args.datadir, "*.csv"))

    for algo in algorithms:
        errordict = {}
        for fl in files:
            basefilename = os.path.basename(fl)
            print(algo, basefilename)
            df = pd.read_csv(fl)
            dfmat = df.as_matrix()
            splitindex = int(dfmat.shape[0] * args.trainsplit)
            trainmat, testmat = dfmat[:splitindex, :], dfmat[splitindex: , :]
            print("create tf session")
            windowed_train, _, _ = scale_transform(rolling_window(trainmat, args.win))
            windowed_test, wtmin, wtgap= scale_transform(rolling_window(testmat, args.win))
            # windowed_df = np.concatenate([windowed_train, windowed_test], axis=0)
            windowed_df, wfmin, wfgap = scale_transform(rolling_window(dfmat, args.win))
            with tf.Session() as sess:
                model = globals()['train_{}'.format(algo.__name__)](windowed_train.reshape([windowed_train.shape[0], -1]), sess, args.iter,
                                                                    dims=[args.win * dfmat.shape[1]] + args.dims, l1param=args.l1param) 
                if algo.__name__ != 'Deep_Autoencoder':
                    reconstructed = model.AE.getRecon(windowed_df.reshape([windowed_df.shape[0], -1]), sess)
                else:
                    reconstructed = model.getRecon(windowed_df.reshape([windowed_df.shape[0], -1]), sess)
               
                if not os.path.exists(os.path.join(args.outdir, "{}_{}".format(algo.__name__, 
                                                                               to_str(args.dims)))):
                    os.makedirs(os.path.join(args.outdir, "{}_{}".format(algo.__name__,
                                                                         to_str(args.dims))))
                
               
                # ipdb.set_trace()
                # reshape result mat to get original shape
                recon_reshaped = reverse_scale(reconstructed.reshape([-1, args.win, dfmat.shape[1]]), wfmin, wfgap)
                errormat = (recon_reshaped - windowed_df)
                print("Recon.MSE error for algo {}: {}".format(algo.__name__, 
                    (errormat**2).mean()))
                
                print("Recon.ABS error for algo {}: {}".format(algo.__name__, 
                    (np.abs(errormat)).mean()))
                
                df.loc[:, :] = np.concatenate([recon_reshaped[0, :, :], recon_reshaped[1:, -1, :]], axis=0)
                
                out_fname = os.path.join(args.outdir, "{}_{}".format(algo.__name__, to_str(args.dims)),
                                         basefilename)
                df.to_csv(out_fname, index=False)
                errordict[basefilename] = {"mean": np.mean(errormat ** 2), "max": np.max((errormat ** 2).mean(axis=1))}
                
                # save tensorflow model
                saver = tf.train.Saver()
                modeloutdir = os.path.join(args.outdir, "model")
                try:
                    os.makedirs(modeloutdir)
                except:
                    pass

                saver.save(sess, modeloutdir, global_step=1000) #, max_to_keep=2)

        with open(os.path.join(args.outdir, "{}_{}".format(algo.__name__, to_str(args.dims)), "performance.txt"), "w") as outf:
            outf.write(json.dumps(errordict))


if __name__ == "__main__":
    import argparse
    from datetime import datetime
    ap = argparse.ArgumentParser()
    ap.add_argument("--outdir", help='output directory', 
                    default="../../results/autoencoder_{}".format(datetime.now().strftime('%m%d%H%M')))
    ap.add_argument("--datadir", help='input data directory', default='../../data/ieee_33_bus_dataset/')
    ap.add_argument("--iter", type=int, help='number of epochs to run')
    ap.add_argument("--trainsplit", type=float, help="train percentage")
    ap.add_argument("--algo", help="which algorithm to create")
    ap.add_argument("--win", type=int, help="window size")
    ap.add_argument("--dims", help='nn dimensions', type=int, nargs="+", default=[32, 20])
    ap.add_argument("--l1param", help='l1parameter', type=int, default=10)
    args = ap.parse_args()

    main(args)
