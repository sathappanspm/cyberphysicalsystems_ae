#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
    *.py: Description of what * does.
    Last Modified:
"""

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"

import numpy as np
import pandas as pd
from RDAE import Deep_Autoencoder, RobustL21Autoencoder, RDAE
import os
import tensorflow as tf
import glob
import json


def train_Deep_Autoencoder(data, sess, num_epochs, dims=None, l1param=None):
    model = Deep_Autoencoder(sess, input_dim_list=dims)
    model.fit(data, sess=sess, learning_rate=0.01,
              batch_size=data.shape[0], verbose=False, iteration=num_epochs)
    return model


def train_RobustL21Autoencoder(data, sess, num_epochs, dims=None, l1param=None):
    model = RobustL21Autoencoder(sess=sess, lambda_=l1param, layers_sizes=dims)
    model.fit(data, sess=sess, inner_iteration=num_epochs,
              iteration=5, batch_size=data.shape[0], verbose=False)
    return model


def train_RDAE(data, sess, num_epochs, dims=None, l1param=None):
    model = RDAE(sess=sess, lambda_=l1param, layers_sizes=dims)
    model.fit(data, sess=sess, learning_rate=0.01, batch_size=data.shape[0],
              inner_iteration=num_epochs, iteration=5, verbose=False)
    return model


def main(args):
    if args.algo == 'all':
        algorithms = [Deep_Autoencoder, RobustL21Autoencoder, RDAE]
    else:
        algorithms = [globals()[args.algo]]

    files = glob.glob(os.path.join(args.datadir, "*.csv"))

    for algo in algorithms:
        errordict = {}
        for fl in files:
            basefilename = os.path.basename(fl)
            print(algo, basefilename)
            df = pd.read_csv(fl)
            dfmat = df.as_matrix()
            splitindex = int(dfmat.shape[1] * args.trainsplit)
            trainmat, testmat = dfmat[:splitindex, :], dfmat[splitindex: , :]
            print("create tf session")
            with tf.Session() as sess:
                model = globals()['train_{}'.format(algo.__name__)](trainmat, sess, args.iter,
                                                                    dims=args.dims, l1param=args.l1param) 
                if algo.__name__ != 'Deep_Autoencoder':
                    reconstructed = model.AE.getRecon(dfmat, sess)
                else:
                    reconstructed = model.getRecon(dfmat, sess)
               
                if not os.path.exists(os.path.join(args.outdir, "{}_{}-{}".format(algo.__name__, *args.dims))):
                    os.makedirs(os.path.join(args.outdir, "{}_{}-{}".format(algo.__name__, *args.dims)))
                
                errormat = (reconstructed[splitindex:, :] - dfmat[splitindex:, :])
                print("Recon.MSE error for algo {}: {}".format(algo.__name__, 
                    (errormat**2).mean()))
                
                print("Recon.ABS error for algo {}: {}".format(algo.__name__, 
                    (np.abs(errormat)).mean()))
               
                df.loc[:, :] = reconstructed
                out_fname = os.path.join(args.outdir, "{}_{}-{}".format(algo.__name__, *args.dims), basefilename)
                df.to_csv(out_fname, index=False)
                errordict[basefilename] = {"mean": np.mean(errormat ** 2), "max": np.max((errormat ** 2).mean(axis=1))}

        with open(os.path.join(args.outdir, "{}_{}-{}".format(algo.__name__, *args.dims), "performance.txt"), "w") as outf:
            outf.write(json.dumps(errordict))


if __name__ == "__main__":
    import argparse
    from datetime import datetime
    ap = argparse.ArgumentParser()
    ap.add_argument("--outdir", help='output directory', 
                    default="../../results/autoencoder_{}".format(datetime.now().strftime('%m%d%H%M')))
    ap.add_argument("--datadir", help='input data directory', default='../../data/ieee_33_bus_dataset/')
    ap.add_argument("--iter", type=int, help='number of epochs to run')
    ap.add_argument("--trainsplit", type=float, help="train percentage")
    ap.add_argument("--algo", help="which algorithm to create")
    ap.add_argument("--dims", help='nn dimensions', type=int, nargs="+", default=[32, 20])
    ap.add_argument("--l1param", help='l1parameter', type=int, default=10)
    args = ap.parse_args()
    main(args)
